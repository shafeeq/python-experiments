from time import sleep
import codecs
import twitter

auth={'consumer_key': '',
      'consumer_secret': '',
      'access_token_key': '',
      'access_token_secret': ''}

t = twitter.Api(**auth)
fetch_count = 1
while True:
    print "Fetch #%d" % fetch_count
    tweets = t.GetFriendsTimeline()
    tweets.reverse()
    fin = codecs.open('tweetsfile', encoding='utf-8', mode='r')
    existing_tweets = fin.readlines()
    for tweet in tweets:
        stripped_tweet = tweet.user.screen_name + u' : ' + tweet.text + u'\n'
        #print stripped_tweet #debug
        try:
            if existing_tweets.index(stripped_tweet) >= 0:
                continue
        except ValueError:
                fout = codecs.open('tweetsfile', encoding='utf-8', mode='a+')
                fout.write(stripped_tweet)
                fout.close()
   
    fin.close()
    fetch_count += 1
    sleep(15)
