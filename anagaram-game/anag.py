import random
import os
f = open('anag').readlines()
w = []
for i in f:
    w.append(i[:-1])
wdict = {}
for i in w:
    j = list(i)
    wdict[i] = j
    random.shuffle(wdict[i])

print"Welcome to ANAGRAM Game!!"
print"Enter answers to get scores"
print "Enter PASS or pass to skip current question"
score = 0
def getWordAndClue():
    while True:
        word,anagram = random.sample(wdict.items(),1)[0]
        if word == ''.join(anagram):
            pass
        elif word == 'PASS':
            pass
        else:
            return word,anagram
while True:
    word,anagram  = getWordAndClue() 
    print "Clue : ",
    for i in anagram: print i+' ',
    print '\n'
    while True:
        ans = raw_input("Your answer: ")
        if ans.upper() == 'PASS':
            break
        if ans.upper() != word: print"Wrong answer. Try again"
        else:
            break
    if ans.upper() != 'PASS':
        score = score + 1
        print "Right answer! Your Score = %d" %score
    else:
        print "Question skipped. The answer was %s" %word

