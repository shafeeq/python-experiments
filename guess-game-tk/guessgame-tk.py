from Tkinter import *
import random

class GuessGame:
    def __init__(self,master=0):
        
        #create GUI
        self.mainWindow = Frame(master)
        self.mainWindow.master.title("Guessing Game")
        self.lInfo= Label(self.mainWindow,text='Welcome to Guessing Game!\nThe Computer has in its mind, a number between 1 and 100\nEnter your guess:')
        self.lInfo.pack(side=TOP)
        

        self.Fentry = Frame(self.mainWindow)
        self.entry = Entry(self.Fentry)
        self.entry.pack(side=TOP,fill=X)
        self.entry.bind("<Return>",self.evEnter)
        self.entry.bind("<KP_Enter>",self.evEnter) #for numpad enter
        self.lResult = Label(self.Fentry,text="")
        self.lResult.pack(side=BOTTOM)
        self.Fentry.pack()

        self.Fbuttons = Frame(self.mainWindow,border = 1, relief="groove")
        self.bNewgame  =Button(self.Fbuttons, text = "New Game", command = self.newgame)
        self.bNewgame.pack(side=LEFT,padx=15)
        self.bQuit = Button(self.Fbuttons, text="Quit", command=self.mainWindow.quit)
        self.bQuit.pack(side=RIGHT,padx=15)
        self.Fbuttons.pack(side=TOP,fill=X)

        self.mainWindow.pack() 

        #init newgame
        self.newgame()

    def newgame(self):
        #create the random number
        self.lResult['text']=""
        self.rndno = random.randint(1,101)

    def evEnter(self,event):
        self.num = int(self.entry.get())
        self.check(self.num)
        self.entry.delete(0,END)
    

    def check(self,num):
        if num < self.rndno:
            self.lResult['text'] = "Try higher"
        elif num > self.rndno:
            self.lResult['text'] = "Try smaller"
        elif num == self.rndno:
            self.lResult['text'] = "%d is the right answer! Congrats!" %num
         


app = GuessGame()
app.mainWindow.mainloop()
