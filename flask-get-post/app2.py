# -*- coding: utf-8 -*-
import requests as req
from flask import Flask, request, render_template

app = Flask(__name__)
app.config['DEBUG'] = True
sent = []
received = []
@app.route('/', methods=['GET', 'POST'])
def index():
	if request.method == 'GET':
		return render_template('index.html', sent=sent, received=received)
	elif request.method == 'POST':
		sent.append(
			{'user':request.form['user'],
			'message':request.form['message']})
		r = req.post('http://localhost:1111/messenger',data=sent[-1])
		return render_template('index.html', sent=sent, received=received)

@app.route('/messenger', methods=['POST'])
def receive():
	received.append(dict(user=request.form['user'], message=request.form['message']))#({'user':request.form['user'], 'message':request.form['message']})
	return 'Success'

if __name__ == '__main__':
	app.run(port=2222)
