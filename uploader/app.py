# -*- coding: utf-8 -*-
import os
from flask import Flask, request, render_template, flash, redirect, url_for, send_file, session
from werkzeug.utils import secure_filename

app = Flask(__name__)
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024
app.config['DEBUG']=True	
app.secret_key = 'to be or no to be'

@app.route('/', defaults={'path': ''},methods=['GET','POST'])
@app.route('/<path:path>',methods=['GET','POST'])
def index(path):
	if request.method == 'GET':
		if os.path.isfile(path):
			send_from_directory(os.path.dirname(path),
			             os.path.basename(path))

			return
			return redirect(url_for('index',path=os.path.dirname(path)))

		default_path = '/home/shafeeq'
		if not path or path.find(default_path[1:]) == -1:# or not os.path.exists(path):
			path = default_path
		else:
			path = '/'+ path
		if not path == default_path:
			prev = os.path.abspath(path + '/..')
		else:
			prev = path
		oslist = os.listdir(path)
		dirlist = []
		filelist = []
		sizelist = []
		for i in oslist:
			if not i[0] == '.':
				if os.path.isdir(os.path.join(path,i)):
					dirlist.append(i)
				else:
					filelist.append(i)
					sizelist.append(sizeof_fmt(os.path.getsize(path+'/'+i)))

		dirlist  = sorted(dirlist,key=lower)
		filesinfo = zip(filelist,sizelist)
		filesinfo=  sorted(filesinfo,key=firstitem)
		return render_template('index.html', dirlist=dirlist, filesinfo=filesinfo, path=path, prev=prev)

	elif request.method == 'POST':
		file = request.files['filename']
		if file:
			filename = secure_filename(file.filename)
			file.save(os.path.join(request.form['prev_path'], filename))
			flash(u'File "%s" was uploaded successfully.' % filename, 'success')
			return redirect(url_for('index',path=request.form['prev_path']))
		else:
			flash(u'File could not be uploaded.', 'danger')
			return redirect(url_for('index',path=request.form['prev_path']))

def sizeof_fmt(num):
    for x in [' B','KB','MB','GB','TB']:
        if num < 1024.0:
            return "%3.1f %s" % (num, x)
        num /= 1024.0

def firstitem(i):
	return i[0].lower()
def lower(i):
	return i.lower()


@app.before_request
def csrf_protect():
    if request.method == "POST":
        token = session.pop('_csrf_token', None)
        if not token or token != request.form.get('_csrf_token'):
            abort(403)

def generate_csrf_token():
    if '_csrf_token' not in session:
        session['_csrf_token'] = 'dfsdfdsfsdfsd'
    return session['_csrf_token']

app.jinja_env.globals['csrf_token'] = generate_csrf_token 

# @app.route('/upload', methods=['POST'])
# def upload():
# 	file = request.files['secure_filename']
# 	if file:
# 		filename = secure_filename(file.filename)
# 		file.save(os.path.join('/home/shafeeq/new/', filename))
# 	#return "hello"
# 	else:
# 		return "Could not find/access the file."
if __name__ == '__main__':
	app.run()

