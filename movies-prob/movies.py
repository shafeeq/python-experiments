import json
f = open('movies.json').read()
j = json.loads(f)
genre = {}
for i in j['movies']:
    for k in i['genres']:
        if not k in genre:
            genre[k] = {'count':0, 'total':0}
        genre[k]['count'] += 1
        genre[k]['total'] += i['rating']


for g in sorted(genre.keys()):
    print("%.2f %s") %(genre[g]['total']/genre[g]['count'], g)
