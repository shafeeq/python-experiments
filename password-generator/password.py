from random import randint,shuffle,sample

number_count = int(raw_input("Integers?\n> "))
alpha_count = int(raw_input("Alphabets?\n> "))

password = []

for i in range(0, number_count):
    password.append(str(randint(0,9)))

alphabets="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
# for seperate numbers of small and capital letters, split 'alphabtes'
# and run the iterator on both of them
for i in range(0, alpha_count):
    password.append(sample(alphabets,1)[0])


print ''.join(password)
shuffle(password)
print ''.join(password)


