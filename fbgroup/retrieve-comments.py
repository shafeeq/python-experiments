#!/usr/bin/python
#usage: python retrieve-comments.py file id limit access_token
#retrieves comments of id from facebook graph and saves to file
import sys
import urllib
import json
file,id,limit,token = sys.argv[1:]

url = "https://graph.facebook.com/" + id + "/comments?limit=" + limit\
        + "&access_token=" + token
data = urllib.urlopen(url).read()
j = json.loads(data)
if 'error' in j.keys():
    print "Error:",j['error']['type']
    print j['error']['message']
    sys.exit()
f = open(file,'w')
f.write(data)
f.close()

